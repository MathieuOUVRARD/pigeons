# Pigeons

Consigne: Le but est d’implémenter une simulation d’alimentation de pigeon dans un espace public. Le jeu se passe dans une fenêtre où les pigeons attendent la nourriture. L’utilisateur, alors, leur donne à manger en cliquant sur un emplacement.